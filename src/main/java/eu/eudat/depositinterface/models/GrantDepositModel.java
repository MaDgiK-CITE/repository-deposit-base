package eu.eudat.depositinterface.models;

import java.util.UUID;

public class GrantDepositModel {
    private UUID id;
    private String reference;
    private FunderDepositModel funder;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }
    public void setReference(String reference) {
        this.reference = reference;
    }

    public FunderDepositModel getFunder() {
        return funder;
    }
    public void setFunder(FunderDepositModel funder) {
        this.funder = funder;
    }
}
