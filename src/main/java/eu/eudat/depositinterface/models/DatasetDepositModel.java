package eu.eudat.depositinterface.models;

import java.util.List;

public class DatasetDepositModel {
    private String label;
    private String description;
    private String profileDefinition;
    private String properties;
    private List<DatasetFieldsDepositModel> fields;

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getProfileDefinition() {
        return profileDefinition;
    }
    public void setProfileDefinition(String profileDefinition) {
        this.profileDefinition = profileDefinition;
    }

    public String getProperties() {
        return properties;
    }
    public void setProperties(String properties) {
        this.properties = properties;
    }

    public List<DatasetFieldsDepositModel> getFields() {
        return fields;
    }
    public void setFields(List<DatasetFieldsDepositModel> fields) {
        this.fields = fields;
    }
}
