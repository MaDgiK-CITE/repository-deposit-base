package eu.eudat.depositinterface.models;

public class OrganisationDepositModel {
    private String label;

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
}
