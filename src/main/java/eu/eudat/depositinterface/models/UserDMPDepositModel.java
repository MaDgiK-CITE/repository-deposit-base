package eu.eudat.depositinterface.models;

public class UserDMPDepositModel {

    public enum UserDMPRoles {
        OWNER(0), USER(1);

        private int value;

        UserDMPRoles(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }


        public static UserDMPRoles fromInteger(int value) {
            switch (value) {
                case 0:
                    return OWNER;
                case 1:
                    return USER;
                default:
                    throw new RuntimeException("Unsupported User Dmp Role Message Code");
            }
        }
    }

    private UserInfoDepositModel user;
    private Integer role;

    public UserInfoDepositModel getUser() {
        return user;
    }
    public void setUser(UserInfoDepositModel user) {
        this.user = user;
    }

    public Integer getRole() {
        return role;
    }
    public void setRole(Integer role) {
        this.role = role;
    }
}
