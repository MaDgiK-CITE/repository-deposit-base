package eu.eudat.depositinterface.models;

import java.io.File;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class DMPDepositModel {
    private UUID id;
    private int version;
    private String label;
    private String description;
    private boolean isPublic;
    private List<DatasetDepositModel> datasets;
    private Set<UserDMPDepositModel> users;
    private Set<OrganisationDepositModel> organisations;
    private Set<ResearcherDepositModel> researchers;
    private GrantDepositModel grant;
    private FileEnvelope pdfFile;
    private FileEnvelope rdaJsonFile;
    private File supportingFilesZip;
    private String previousDOI;
    private String extraProperties;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }
    public void setVersion(int version) {
        this.version = version;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isPublic() {
        return isPublic;
    }
    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public List<DatasetDepositModel> getDatasets() {
        return datasets;
    }
    public void setDatasets(List<DatasetDepositModel> datasets) {
        this.datasets = datasets;
    }

    public Set<UserDMPDepositModel> getUsers() {
        return users;
    }
    public void setUsers(Set<UserDMPDepositModel> users) {
        this.users = users;
    }

    public Set<OrganisationDepositModel> getOrganisations() {
        return organisations;
    }
    public void setOrganisations(Set<OrganisationDepositModel> organisations) {
        this.organisations = organisations;
    }

    public Set<ResearcherDepositModel> getResearchers() {
        return researchers;
    }
    public void setResearchers(Set<ResearcherDepositModel> researchers) {
        this.researchers = researchers;
    }

    public GrantDepositModel getGrant() {
        return grant;
    }
    public void setGrant(GrantDepositModel grant) {
        this.grant = grant;
    }

    public FileEnvelope getPdfFile() {
        return pdfFile;
    }
    public void setPdfFile(FileEnvelope pdfFile) {
        this.pdfFile = pdfFile;
    }

    public FileEnvelope getRdaJsonFile() {
        return rdaJsonFile;
    }
    public void setRdaJsonFile(FileEnvelope rdaJsonFile) {
        this.rdaJsonFile = rdaJsonFile;
    }

    public File getSupportingFilesZip() {
        return supportingFilesZip;
    }
    public void setSupportingFilesZip(File supportingFilesZip) {
        this.supportingFilesZip = supportingFilesZip;
    }

    public String getPreviousDOI() {
        return previousDOI;
    }
    public void setPreviousDOI(String previousDOI) {
        this.previousDOI = previousDOI;
    }

    public String getExtraProperties() {
        return extraProperties;
    }
    public void setExtraProperties(String extraProperties) {
        this.extraProperties = extraProperties;
    }
}
