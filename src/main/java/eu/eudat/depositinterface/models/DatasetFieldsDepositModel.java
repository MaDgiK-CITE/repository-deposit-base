package eu.eudat.depositinterface.models;

import java.util.List;

public class DatasetFieldsDepositModel {
    private String renderStyleType;
    private List<String> schematics;
    private Object value;
    private boolean multiple;

    public String getRenderStyleType() {
        return renderStyleType;
    }
    public void setRenderStyleType(String renderStyleType) {
        this.renderStyleType = renderStyleType;
    }

    public List<String> getSchematics() {
        return schematics;
    }
    public void setSchematics(List<String> schematics) {
        this.schematics = schematics;
    }

    public Object getValue() {
        return value;
    }
    public void setValue(Object value) {
        this.value = value;
    }

    public boolean isMultiple() {
        return multiple;
    }
    public void setMultiple(boolean multiple) {
        this.multiple = multiple;
    }
}
