package eu.eudat.depositinterface.models;

public class FunderDepositModel {
    private String label;

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
}
