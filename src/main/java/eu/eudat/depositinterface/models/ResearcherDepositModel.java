package eu.eudat.depositinterface.models;

public class ResearcherDepositModel {
    private String label;
    private String reference;

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getReference() {
        return reference;
    }
    public void setReference(String reference) {
        this.reference = reference;
    }
}
