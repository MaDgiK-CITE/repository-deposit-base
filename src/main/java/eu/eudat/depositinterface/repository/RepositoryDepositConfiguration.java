package eu.eudat.depositinterface.repository;

public class RepositoryDepositConfiguration {

    public enum DepositType {
        SystemDeposit(0), UserDeposit(1), BothWaysDeposit(2);

        private int value;

        DepositType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static DepositType fromInteger(int value) {
            switch (value) {
                case 0:
                    return SystemDeposit;
                case 1:
                    return UserDeposit;
                case 2:
                    return BothWaysDeposit;
                default:
                    throw new RuntimeException("Unsupported Deposit Account Type");
            }
        }
    }

    private int depositType;
    private String repositoryId;
    private String accessToken;
    private String repositoryUrl;
    private String repositoryAuthorizationUrl;
    private String repositoryRecordUrl;
    private String repositoryAccessTokenUrl;
    private String repositoryClientId;
    private String repositoryClientSecret;
    private String redirectUri;
    private boolean hasLogo;

    public int getDepositType() {
        return depositType;
    }
    public void setDepositType(int depositType) {
        this.depositType = depositType;
    }

    public String getRepositoryId() {
        return repositoryId;
    }
    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getAccessToken() {
        return accessToken;
    }
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRepositoryUrl() {
        return repositoryUrl;
    }
    public void setRepositoryUrl(String repositoryUrl) {
        this.repositoryUrl = repositoryUrl;
    }

    public String getRepositoryAuthorizationUrl() {
        return repositoryAuthorizationUrl;
    }
    public void setRepositoryAuthorizationUrl(String repositoryAuthorizationUrl) {
        this.repositoryAuthorizationUrl = repositoryAuthorizationUrl;
    }

    public String getRepositoryRecordUrl() {
        return repositoryRecordUrl;
    }
    public void setRepositoryRecordUrl(String repositoryRecordUrl) {
        this.repositoryRecordUrl = repositoryRecordUrl;
    }

    public String getRepositoryAccessTokenUrl() {
        return repositoryAccessTokenUrl;
    }
    public void setRepositoryAccessTokenUrl(String repositoryAccessTokenUrl) {
        this.repositoryAccessTokenUrl = repositoryAccessTokenUrl;
    }

    public String getRepositoryClientId() {
        return repositoryClientId;
    }
    public void setRepositoryClientId(String repositoryClientId) {
        this.repositoryClientId = repositoryClientId;
    }

    public String getRepositoryClientSecret() {
        return repositoryClientSecret;
    }
    public void setRepositoryClientSecret(String repositoryClientSecret) {
        this.repositoryClientSecret = repositoryClientSecret;
    }

    public String getRedirectUri() {
        return redirectUri;
    }
    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    public boolean isHasLogo() {
        return hasLogo;
    }
    public void setHasLogo(boolean hasLogo) {
        this.hasLogo = hasLogo;
    }
}
