package eu.eudat.depositinterface.repository;

import eu.eudat.depositinterface.models.DMPDepositModel;

import java.util.List;

/**
 * The RepositoryDeposit interface represents the mechanism of depositing a dmp to any
 * repository which mints a persistent digital object identifier (DOI) for each submission,
 * which makes the stored dmps easily citeable.
 */
public interface RepositoryDeposit {

    /**
     * Returns a string representing the persistent digital object identifier (DOI) which
     * was created.
     *
     * @param repositoryId the id of the repository
     * @param dmpDepositModel dmp structure which is to be deposited
     * @param repositoryAccessToken access token needed for the authentication to the repository
     *                              if this is null, another authentication mechanism is used for
     *                              that repository e.g. api token which will be fetched from the
     *                              repository's configuration
     * @return a string representing the persistent digital object identifier (DOI)
     * @throws Exception if an error occurs while trying to deposit the dmp
     */
    String deposit(String repositoryId, DMPDepositModel dmpDepositModel, String repositoryAccessToken) throws Exception;

    /**
     * Returns the access token from the oauth2 flow.
     *
     * @param repositoryId the id of the repository
     * @param code oauth2 authorization code
     * @return the access token or null if no oauth2 protocol is not supported for that repository
     */
    String authenticate(String repositoryId, String code);

    /**
     * Returns the repository's configuration.
     *
     * @return List of structure representing the configuration of the repository <br>
     *         which includes the following fields: <br>
     *         depositType - an integer representing how the dmp user can deposit in the repository,
     *         0 stands for system deposition meaning the dmp is deposited using argos credentials to the
     *         repository, 1 stands for user deposition in which the argos user specifies his/her own credentials
     *         to the repository, 2 stands for both ways deposition if the repository allows the deposits of dmps
     *         to be made from both argos and users accounts<br>
     *         repositoryId - unique identifier for the repository<br>
     *         accessToken - access token provided for the system type deposits<br>
     *         repositoryUrl - repository url<br>
     *         repositoryAuthorizationUrl - repository's authorization url<br>
     *         repositoryRecordUrl - repository's record url, this url is used to index dmps that are created<br>
     *         repositoryAccessTokenUrl - repository's access token url<br>
     *         repositoryClientId - repository's client id<br>
     *         repositoryClientSecret - repository's client secret<br>
     *         redirectUri - redirect uri to argos after the oauth2 flow from the repository<br>
     */
    List<RepositoryDepositConfiguration> getConfiguration();

    /**
     * Returns the repository's logo if exists.
     *
     * @param repositoryId the id of the repository
     * @return the repository's logo in base64 form
     * */
    String getLogo(String repositoryId);

}
