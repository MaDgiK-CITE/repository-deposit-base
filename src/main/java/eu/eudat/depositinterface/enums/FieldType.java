package eu.eudat.depositinterface.enums;

public enum FieldType {
    COMBO_BOX("combobox"),
    BOOLEAN_DECISION("booleanDecision"),
    RADIO_BOX("radiobox"),
    INTERNAL_DMP_ENTRIES("internalDmpEntities"),
    CHECK_BOX("checkBox"),
    FREE_TEXT("freetext"),
    TEXT_AREA("textarea"),
    RICH_TEXT_AREA("richTextarea"),
    UPLOAD("upload"),
    TABLE("table"),
    DATE_PICKER("datePicker"),
    EXTERNAL_DATASETS("externalDatasets"),
    DATA_REPOSITORIES("dataRepositories"),
    JOURNAL_REPOSITORIES("journalRepositories"),
    PUB_REPOSITORIES("pubRepositories"),
    LICENSES("licenses"),
    TAXONOMIES("taxonomies"),
    PUBLICATIONS("publications"),
    REGISTRIES("registries"),
    SERVICES("services"),
    TAGS("tags"),
    RESEARCHERS("researchers"),
    ORGANIZATIONS("organizations"),
    DATASET_IDENTIFIER("datasetIdentifier"),
    CURRENCY("currency"),
    VALIDATION("validation");

    private final String name;

    FieldType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static FieldType fromName(String name) {
        for (FieldType fieldType : FieldType.values()) {
            if (name.equals(fieldType.getName())) {
                return fieldType;
            }
        }
        throw new IllegalArgumentException("View Style Type [" + name + "] is not valid");
    }
}
